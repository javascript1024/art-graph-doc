---
title: 自定义 Widget
metaTitle: "自定义 Widget"
metaDescription: "自定义 Widget"
---

<iframe src="https://codesandbox.io/embed/zi-ding-yi-widget-8dh98?fontsize=14&hidenavigation=1&theme=dark&view=preview"
     style="width:100%; height:800px; border:0; border-radius: 4px; overflow:hidden;"
     title="自定义 Widget"
     allow="accelerometer; ambient-light-sensor; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; vr; xr-spatial-tracking"
     sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"
   ></iframe>
