---
title: 开发指南
metaTitle: "开发指南"
---

Art Graph 是一款基于 TypeScript 和 Web GL 开发的绘图库，它可以快速创建在线绘制线框图程序。


## 安装

```bash
npm i art-graph-core
```

## tsconfig 配置

```ts
  "compilerOptions": {
      "esModuleInterop": true
  }
```

## 特性

- ✔︎ 高性能
- ✔︎ 交互体验好
- ✔︎ 代码扩展性好
- ✔︎ 强大的扩展能力


## Hello World

创建一个便利贴

<iframe src="https://codesandbox.io/embed/hello-world-q9rtq?fontsize=14&hidenavigation=1&theme=dark&view=preview"
     style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;"
     title="Hello World"
     allow="accelerometer; ambient-light-sensor; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; vr; xr-spatial-tracking"
     sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"
   ></iframe>
