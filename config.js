const config = {
  gatsby: {
    pathPrefix: '/',
    siteUrl: 'https://www.beeart.com',
    gaTrackingId: null,
    trailingSlash: false,
  },
  header: {
    logo: 'https://www.beeart.com/favicon.ico',
    logoLink: 'https://www.beeart.com',
    title: 'Art Graph',
    githubUrl: '',
    helpUrl: '',
    tweetText: '',
    social: '',
    links: [{ text: '', link: '' }],
    search: {
      enabled: false,
      indexName: '',
      algoliaAppId: process.env.GATSBY_ALGOLIA_APP_ID,
      algoliaSearchKey: process.env.GATSBY_ALGOLIA_SEARCH_KEY,
      algoliaAdminKey: process.env.ALGOLIA_ADMIN_KEY,
    },
  },
  sidebar: {
    forcedNavOrder: ['/getting-started', '/architecture-design', '/development-specification'],
    collapsedNav: [],
    links: [],
    frontline: false,
    ignoreIndex: true,
    title: '目录',
  },
  siteMetadata: {
    title: 'Art Graph',
    description: '高性能在线白板引擎',
    ogImage: null,
    docsLocation: '',
    favicon: 'https://www.beeart.com/favicon.ico',
  },
  pwa: {
    enabled: false, // disabling this will also remove the existing service worker.
    manifest: {
      name: 'Gatsby Gitbook Starter',
      short_name: 'GitbookStarter',
      start_url: '/',
      background_color: '#6b37bf',
      theme_color: '#6b37bf',
      display: 'standalone',
      crossOrigin: 'use-credentials',
      icons: [
        {
          src: 'src/pwa-512.png',
          sizes: `512x512`,
          type: `image/png`,
        },
      ],
    },
  },
};

module.exports = config;
