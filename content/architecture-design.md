---
title: 架构设计
metaTitle: "架构设计"
metaDescription: "架构设计"
---

## 技术栈

- WebGL
- TypeScript
- requestAnimationFrame

## 组件图

<img src="/images/architecture-design1.png" />

### Art Graph 主要分成三大模块

- Core: 基于 Pixi.js 操作 WebGL, 并且扩展基本公共功能（如：Camera,Keyboard,Mouse），给上层 Widget 与 Shape 提供支持。
- Shape: 绘制在白板上的元素，**Shape 感知不到 Widget 的存在。**
- Widget: 丰富 Graph 的功能，基于接口开发，可插拔，可被替换。


### Shape 的设计

`Shape` 类是 继承于 Pixi.js 的 `Container`, 是所有白板元素的基础，Shape 封装了元素的 `拖拽`, `单击`, `双击` 等交互行为，还有实现 `setUpSize`,`offset`, `setText` 等方法。`Shape` 是一个抽象类，具体元素长什么样子，由子类实现，代码如下：

```ts
import { VectorShape } from './VectorShape';

export class RectangleShape extends VectorShape {
  protected setBackgroundSize(width: number, height: number) {
    this.background.clear();
    this.background.lineStyle(this.style.boardWidth, this.style.boardColor, this.style.boardAlpha, 0);
    this.background.beginFill(this.style.fillColor, this.style.fillAlpha);
    this.background.drawRect(0, 0, width, height);
    this.background.endFill();
    this.setTextMaskSize();
  }
}
```

VectorShape 也是由 Shape 派生出来的，Shape 类派生关系如下

<img src="/images/architecture-design3.png" />



### 数据驱动更新

之前使用 [mx-graph](https://jgraph.github.io/mxgraph/) 想要更新图形，我们首先需要修改 `mxState` 里的数据，其次触发 `graph.refresh()` 方法, 对数据进行一次脏检查，最后更新对应的 SVGElement 的属性。如果用户通过鼠标对大量的元素进行移动，放大等操作，就会导致在短时间内对大量的元素进行修改操作，进而影响渲染性能。

在现在的架构中，我们只需要修改 `Shape` 类，的 `width`, `height`, `x`, `y` 属性，`Pixi.js` 会通过 [requestAnimationFrame](https://developer.mozilla.org/zh-CN/docs/Web/API/Window/requestAnimationFrame) 机制在下一帧去触发更新 WebGL, 这样做优点有两个

- 开发人员不用显示的去调用 `graph.refresh()` 方法来触发更新，开发高性能代码更简单;
- 更新时机由 [requestAnimationFrame](https://developer.mozilla.org/zh-CN/docs/Web/API/Window/requestAnimationFrame) 控制，这样可以有效控制更新频率;

<img src="/images/architecture-design2.png" />

### Widget 的设计

<img src="/images/architecture-design4.png" />

Widget 是面向接口开发，这样可以通过 WidgetsManager [替换 Widget](./aaa) 实现，Widget 通过 WidgetHandler 对 Shape 行为进行监听，并对事件做过响应， Widget 对 Shape 是透明的，Shape 不用知道 Widget 的存在，这样可以降低 Shape 开发的难度。 Shape -> WidgetHandler -> IWidget -> Widget 这样就统一了 Shape 到 Widget 的事件流程。


## 目录说明

```bash
lib/
  camera/         摄像机功能的实现，可以放大，缩小，移动摄像机视口
  core/           目前只实现了 SelectionModel ，用于记录被选中的 Shape 信息
  device/         获取浏览器的信息和特性，比如是否支持全屏，浏览器版本等等
  dom/            封装对 `dom` 的操作
  geometry/       主要是把 Shape 上的几何问题抽象到 Geometry 里实现，如图形碰撞，向量操作，设置图形的大小
  input/          封装了 鼠标，键盘，触摸屏等输入
  math/           封装更通用的数学问题，如两点距离
  scale/          缩放功能的实现
  settings/       设置各 Shape 与 Widgets 的默认值
  shape/          白板上元素的实现
  shortcut-key/   快捷键的实现与管理
  structs/        也是几何模型，可与 Shape 没有对应关系，所以就放到这个目录里
  type-defs/      （这个目录待调整）
  utils/          工具函数
  widgets/        Widget 模块的实现
```
