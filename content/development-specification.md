---
title: 开发规范
metaTitle: "开发规范"
---

## 目录与文件

- 目录小写，用中划线分隔（如：`line-shape`，`shortcut-key`）
- 文件名首字母大小，用驼峰命名（如：`NoteShape.ts`，`RectangleShape.ts`）
- 如果是文件只包含事件名，则全大写，用下划线分隔, 已 `_EVENT.ts` 结束，如：`SHAPE_CHANGE_SIZE_EVENT.ts`
- index.ts 文件全小写


## 一、代码规范

### 1. 在组件里不能使用箭头函数绑定事件

- 问题：在组件里不能使用箭头函数绑定事件，会导致子类不能调用 `super.xxx()` 方法。

- 建议：用 `bind(this)` 来绑定方法，用法如下

```typescript
class SelectionBox {
  constructor(public graph: Graph) {
    this.initListener();
  }

  private initListener() {
    this.destroy = this.destroy.bind(this);
    this.graph.events.on(DESTROY, this.destroy);
  }

  destroy() {
    super.destroy();
    this.graph.events.off(DESTROY, this.destroy);
  }
}
```

### 2. 特性不要在 Graph.ts 实现，Graph.ts 只是很薄的封装


## 二、Shape 开发

### 1. 常用方法声明

- refreshPosition   刷新位置
- setUpSize   设置大小
- getSize   获取大小
- setTranslate   移动到某位置
- offset   设置要移动的距离
- setTranslateAndSize 移动到某位置和大小

## 三、Widget 开发
